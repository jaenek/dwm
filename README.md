# ![dwm logo](dwm.png "dwm logo")
dwm is an extremely fast, small, and dynamic window manager for X.

## Requirements
In order to build dwm you need the Xlib header files.

## Installation
Edit config.mk to match your local setup (dwm is installed into
the /usr/local namespace by default).

Afterwards enter the following command to build and install dwm (if
necessary as root):
```
make clean install
```

## Running dwm
Add the following line to your .xinitrc to start dwm using startx:
```
exec dwm
```
In order to connect dwm to a specific display, make sure that
the DISPLAY environment variable is set correctly, e.g.:
```
DISPLAY=foo.bar:1 exec dwm
```

(This will start dwm on display :1 of the host foo.bar.)

In order to display status info in the bar, you can do something
like this in your .xinitrc:
```sh
while xsetroot -name "`date` `uptime | sed 's/.*,//'`"
do
    sleep 1
done &
exec dwm
```

## Configuration
The configuration of dwm is done by creating a custom config.h
and (re)compiling the source code.

## Patches
This source was modified and contains following patches:
* dwm-6.1-urg-border.diff
* dwm-alpha-20180613-b69c870.diff
* dwm-center-20160719-56a31dc.diff
* dwm-centeredwindowname-20180909-6.2.diff
* dwm-cyclelayouts-20180524-6.2.diff
* dwm-hide_vacant_tags-6.2.diff
* dwm-movestack-6.1.diff
* dwm-noborder-20170207-bb3bd6f.diff
* dwm-pertag-20170513-ceac8c9.diff
* dwm-savefloats-20181212-b69c870.diff
* dwm-statusbutton-20180524-c8e9479.diff
* dwm-statuscolors-20181008-b69c870.diff
* dwm-sticky-20160911-ab9571b.diff
* dwm-tilegap-6.2.diff